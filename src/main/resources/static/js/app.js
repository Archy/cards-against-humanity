var stompClient = null;
var intervalPingCall;
var intervalPingTime = 1000;
var isConnected = false;
var selected = null;
var ifWhiteCardSent = false;
var isTzar = false;
var isFinished = false;
var tzarLeft = false;
var isWinner = false;
var currentLatency = 0;
var latencyHistory = [];
var timeLeft;
var roundTimer;
var roundTimerStopper;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    $("#login-text").prop("disabled", !connected);
    if (connected) {
        $("#disconnect").removeClass("invisible");
        $("#row-connect").hide("slow");
        $("#row-name").show("slow");
        //Development purpose only
        //$("#row-logs").show("slow");
    }
    else {
        $("#disconnect").addClass("invisible");
        $("#row-name").hide("slow");
        $("#row-connect").show("slow");
        $("#row-logs").hide("slow");
        $("#row-wait").hide("slow");
        $("#row-game").hide("slow");
        $("#lost").hide();
        $("#win").hide();
        $("#result-msg").hide();
        $("#new-game").hide();
        $("#login-text").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/cah-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/cah/socket', function (response) {
            manageResponse(response);
        });
    });
}

function disconnect() {
    /*isConnected = false;
    sendPings(false);
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);*/
    location.reload();
    console.log("Disconnected");
}
function sendPings(connect) {
    if(connect === true) {
        intervalPingCall = setInterval(function(){
            var serverTimestamp = new Date().getTime();
            stompClient.send("/app/ping", {}, JSON.stringify({'time': String(serverTimestamp),'myCurrentLatency':currentLatency}) );
        }, intervalPingTime);
    }
    else {
        clearInterval(intervalPingCall);
    }
}

function sendName() { 
    stompClient.send("/app/entry", {}, JSON.stringify({'userName': $("#name").val()}));
    isConnected = true;
    $("#login-text").html("Logged as: " + $("#name").val());
}

function sendMessage(address, jsonMessage){
    if(isConnected === false || stompClient === null){
        return;
    }
    stompClient.send(address, {}, jsonMessage);
}

function manageResponse(message) {
    var formattedString;
    try {
        var obj = $.parseJSON(message.body);
        if(typeof obj.type !== 'undefined' && isConnected === true && !isFinished) {
            if(tzarLeft) {
                if(obj.type === "new-round" || obj.type === "new-round-tzar") {
                    tzarLeft = false;
                } else {
                    return;
                }
            }

            formattedString = "Type: " + obj.type + " | Message: " + obj.msg;
            switch(obj.type) {
                case "timeResolution":
                    resolveLatency(obj.time);
                    break;
                case "game-start":
                    startGame();
                    break;
                case "wait":
                    waitForUsers(obj.msg);
                    break;
                case "logged":
                    setLogged(obj.msg);
                    break;
                case "players-list":
                    printPlayers(obj.data);
                    break;
                case "new-card":
                    addCard(obj.data);
                    break;
                case "new-round":
                    isTzar = false;
                    setNewRound(obj.data);
                    break;
                case "new-round-tzar":
                    isTzar = true;
                    setNewRound(obj.data);
                    break;
                case "choose-winner":
                    displayPlayersAnswers(obj.data);
                    break;
                case "players-left":
                    endGame(obj.msg);
                    break;
                case "kicked":
                    endGame(obj.msg);
                    break;
                case "tzar-kicked":
                    tzarKicked();
                    break;
                case "end-game-win":
                    isWinner=true;
                    endGameResult(obj.msg);
                    break;
                case "end-game-lost":
                    isWinner=false;
                    endGameResult(obj.msg);
              }
        }
        else {
            if(obj.type === "timeResolution"){
                formattedString = obj.time;
            }
            else
                formattedString = message.body;
        }
    } catch (e) {
        console.error(e);
    }
    if(!isFinished) {
        var tr = $("<tr>");
        tr.css("display", "none");
        $("<td>").text(formattedString).appendTo(tr);
        tr.prependTo($("#greetings")).show("slow");
    }
}
function calculateCurrentLatency() {
    var dataSorted = latencyHistory.sort(function(a,b){return a - b});
    var lowMiddle = Math.floor((latencyHistory.length - 1 )/2);
    var upperMiddle = Math.ceil((latencyHistory.length - 1 )/2);
    currentLatency = ((dataSorted[lowMiddle] + dataSorted[upperMiddle]) / 2);
}
function resolveLatency(data){
    if(data !== "noUser"){
        var splitted = data.split(":");
        var browserTimestamp = Number(splitted[0]);
        var serverTimestamp = Number(splitted[1]);
        var myCurrentTime = new Date().getTime();
        var latency = (myCurrentTime - browserTimestamp)/2;
        var serverDelta = myCurrentTime - serverTimestamp;
        latency = (latency/2) + serverDelta;
        latencyHistory.push(latency);
        if(latencyHistory.length === 1001)
            latencyHistory.shift();
        else if (latencyHistory.length === 1)
            currentLatency = latency;
        else
            calculateCurrentLatency();
    }
}
function setLogged(value) {
    console.log(value);
    if(value === "yes") {
        sendPings(true);
    }
    else {
        isConnected = false;
        $("#log-failure").empty();
        $("#log-failure").show();
        $("#log-failure").append(value);
    }
}

function printPlayers(players) {
    console.warn(players);
    $("#players").empty();
    $("#players").append("<tr><th>Name</th><th>Points</th></tr>");
    $.each(players, function(index, player) {
        $("#players").append("<tr><th>" + player.name + "</th><th>" + player.points + "</th></tr>");
    });
}

function addCard(card) {
    console.warn(card);
    $("#cards").append("<li class='list-item white-card' id='" + card.id + "'><span class='txtmsg'>" + card.text + "</span></li>");
}

function setNewRound(round){
    ifWhiteCardSent = false;
    console.warn("Got round num: " + round.id + " card: " + round.blackCard.text);
    
    startTimer(30);
    
    $("#black-card").empty();
    $("#black-card").append("<span class='txtmsg'>"+ round.blackCard.text +"</span>");

    $("#tzar-name").text("The tzar is " + round.tzar);

    if(isTzar) {
        $("#cards").hide();
        $("#tzar-name").hide();
        $("#tzarMsg").show();
    } else {
        $("#cards").show();
        $("#tzarMsg").hide();
        $("#tzar-name").show();

        $('.white-card').click(function(){
            if(ifWhiteCardSent){
                console.warn("White card already chosen.");
                return;
            }
            $(".white-card").css('background-color', '');
            $(this).css('background-color', '#ccffba');

            if(selected !== null){
                selected.removeClass("selected");
            }
            selected = $(this);
            selected.addClass("selected");
            $( "#selectButton" ).css("visibility", "visible");
        });
    }
}

function sendWhiteCard(){
    stopTimer();
    if(isTzar) {
        console.error("Tzar doesn't send white card.");
        return;
    }
    if(selected === null){
        console.error("No card selected.");
        return;
    }
    $(selected).remove();
    cardid = selected.attr('id');
    message = JSON.stringify({'carduuid': cardid});
    sendMessage('/app/selected', message);
    ifWhiteCardSent = true;

    $( "#selectButton" ).css("visibility", "hidden");
}

function startGame() {
    $("#row-name").hide("slow");
    $("#row-wait").hide("slow");
    $("#row-game").show("slow");
}

function waitForUsers(numberConnected) {
    $("#row-name").hide("slow");
    $("#row-wait").show("slow");
    $("#row-wait h1").text("We've got only " + numberConnected + " of 4 players in the room. Please wait...");
}

function displayPlayersAnswers(chosenCards) {
    $("#winnerMsg").show();
    startTimer(30);
    $.each(chosenCards, function(uuid, text) {
        $("#answers").append('<li class="list-item white-card card-chosen-by-other-user" data-uuid="' + uuid + '"><span class="txtmsg">' + text + '</span></li>');
    });
    
    $(".card-chosen-by-other-user").css("cursor", "pointer");
    $(".card-chosen-by-other-user").on('click', function() {
        chooseWinner($(this).data("uuid"));
        $("#answers").empty();  
        $("#winnerMsg").hide();
    });
}

function chooseWinner(cardid) {
    message = JSON.stringify({'carduuid': cardid});
    $("#answers").empty();
    sendMessage('/app/selected-winner', message);
}

function startTimer(time) {
    stopTimer();
    timeLeft = time;
    $("#time-left-val").text(timeLeft);
    $("#time-left").show("slow");
    
    
    roundTimer = setInterval(function() {
        if(timeLeft > 1)
            --timeLeft;
    
        $("#time-left-val").text(timeLeft);
    } , 1000);

    roundTimerStopper = setTimeout(function() { 
        clearInterval(roundTimer);
    }, time * 1000);
}

function stopTimer() {
    clearInterval(roundTimer);
    window.clearTimeout(roundTimerStopper);
    $("#time-left-val").text("");
    $("#time-left").hide();
}

function endGame(msg) {
    isFinished = true;

    $("#win").hide();
    $("#lost").hide();

    $("#result-msg").text(msg);
    $("#row-game").hide("slow");
    $("#row-end").show("slow");
}

function endGameResult(msg) {
    isFinished = true;

  //  printPlayers();
    $("#tzar-name").hide("slow");
    $("#tzarMsg").hide("slow");
    $("#result-msg").text(msg);
    $("#allCards").hide("slow");
    $("#row-end").show("slow");
    $("#time-left").hide();

    if (isWinner){
        $("#lost").hide();
        $("#win").show();
    }
    else{
        $("#win").hide();
        $("#lost").show();
    }
}

function tzarKicked() {
    tzarLeft = true;
    if(selected !== null && ifWhiteCardSent){
        console.log(selected);
        console.error({id: $(selected).attr('id'), text: $(selected).text()});
        addCard({id: $(selected).attr('id'), text: $(selected).text()});
    }
}

$(function () {
    $(".row").hide();
    $("#row-end").hide();
    $("#winnerMsg").hide();
    $("#row-header").show("slow");
    $("#row-connect").show("slow");
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#time-left").hide();

    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
    $( "#selectButton" ).click(function(){ sendWhiteCard(); });
});
