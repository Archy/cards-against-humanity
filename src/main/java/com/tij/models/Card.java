package com.tij.models;

import com.tij.models.json.CardJson;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class that will represent card in database
 */
@Entity
@Table(
    name = "CARD"
)
public class Card {
    @Id
    @GeneratedValue
    private long id;    //id in database
    @Column(name = "NAME", nullable = false, unique = true)
    private String text;
    @Enumerated(EnumType.STRING)
    private CardColor color;


    public Card() {
    }

    public Card(String text, CardColor color) {
        this.text = text;
        this.color = color;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }
}
