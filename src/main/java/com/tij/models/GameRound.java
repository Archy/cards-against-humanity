package com.tij.models;

import com.tij.models.json.CardJson;

import java.util.HashMap;

public class GameRound {
    private int id;
    private CardJson blackCard;
    private HashMap<User, CardJson> userCardMap = new HashMap<>();
    private String tzar;
    
    public enum RoundStates {
        INIT,
        ROUND_STARTED,  //Now we need to send black card, white cards and choose a tzar
                        //users have <SELECT_CARD_TIMEOUT> miliseconds to choose their white card.
        CARDS_CHOSEN,   //users have voted (or not) and Tzar is selecting winner in <SELECT_WINNER_TIMEOUT> miliseconds.
        ROUND_END       //tzar has chosen. Prepare and propagate next round
    }
    private RoundStates roundState;
    
    public GameRound(){

    }

    public GameRound(int id){
        this.id = id;
        blackCard = null;
        roundState = RoundStates.INIT;
    }

    public int getId(){
        return this.id;
    }

    public CardJson getBlackCard(){
        return this.blackCard;
    }

    public void setBlackCard(CardJson blackCard){
        this.blackCard = blackCard;
    }

    public RoundStates getRoundState(){
        return this.roundState;
    }

    public void setRoundState(RoundStates state){
        this.roundState = state;
    }

    public HashMap<User, CardJson> getUserCardMap() {
        return userCardMap;
    }

    public void setUserCardMap(HashMap<User, CardJson> userCardMap) {
        this.userCardMap = userCardMap;
    }

    public void relateUserWithCard(User user, CardJson card){
        this.userCardMap.put(user, card);
    }
    
    public void setTzar(String name) {
        this.tzar = name;
    }
    
    public String getTzar() {
        return this.tzar;
    }
    
}
