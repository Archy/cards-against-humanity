package com.tij.models;

import com.tij.models.json.PlayerJson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {
    private String name;
    private int points;
    private LocalDateTime lastPing;
    private int latency;
    private final List<UUID> cards = new ArrayList<>();

    public User(String name, LocalDateTime time) {
        this.name = name;
        this.points = 0;
        this.lastPing = time;
        this.latency = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public LocalDateTime getLastPing() {
        return lastPing;
    }

    public void setLastPing(LocalDateTime lastPing) {
        this.lastPing = lastPing;
    }

    public void addCard(UUID uuid) {
        cards.add(uuid);
    }

    public boolean removeCard(UUID uuid) {
        return cards.remove(uuid);
    }

    public boolean hasCard(UUID uuid) {
        return cards.contains(uuid);
    }

    public List<UUID> getCards() {
        return cards;
    }
    
    @Override
    public String toString() {
      return "(" + this.name + "), " + this.points + " pts " + "lat:" + latency;
    }

    public PlayerJson toJsonModel() {
        return new PlayerJson(name, points);
    }
    
    public void addPoint() {
        this.points++;
    }

    public void setLatency(int late){
        latency = late;
    }
    public int getLatency(){
        return latency;
    }
}
