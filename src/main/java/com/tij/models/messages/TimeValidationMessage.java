package com.tij.models.messages;

public class TimeValidationMessage{
    private String time;
    private int myCurrentLatency;
    private String type;

    public TimeValidationMessage(){
        this.type= "timeResolution";
    }
    public TimeValidationMessage(String time,int currentLat){
        this.time = time;
        this.myCurrentLatency = currentLat;
        this.type = "timeResolution";
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }
    public int getMyCurrentLatency(){
        return myCurrentLatency;
    }
    public void setMyCurrentLatency(int latency){this.myCurrentLatency = latency;}
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return this.type;
    }
}
