package com.tij.models.messages;

public class RoomEntryRequest {
    private String userName;
    private String time;
    public String getUserName() {
        return userName;
    }
    public String getTime(){
        return time;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setTime(String time){this.time = time;}
}
