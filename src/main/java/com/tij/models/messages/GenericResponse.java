package com.tij.models.messages;

public class GenericResponse {
    private String msg;
    private String type;
    private Object data;

    public GenericResponse(String type, String msg) {
        this.msg = msg;
        this.type = type;
    }

    public GenericResponse(String type, Object data) {
        this.type = type;
        this.data = data;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return this.type;
    }
    
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
