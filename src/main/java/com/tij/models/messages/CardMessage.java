package com.tij.models.messages;

import java.util.UUID;

public class CardMessage {
    private UUID carduuid;

    public UUID getCarduuid() {
        return this.carduuid;
    }

    public void setCarduuid(UUID carduuid) {
        this.carduuid = carduuid;
    }
    public void setCarduuid(String carduuid) {
        this.carduuid = UUID.fromString(carduuid);
    }
}
