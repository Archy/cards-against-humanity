package com.tij.models.json;

import com.tij.models.CardColor;

import java.util.UUID;

/**
 * Model representing a {@link com.tij.models.Card}, that is send to user
 */
public class CardJson {
    private UUID id;        //random id, generated before sending card to user for the first time in game
    private String text;
    private CardColor color;


    public CardJson() {
    }

    public CardJson(UUID id, String text, CardColor color) {
        this.id = id;
        this.text = text;
        this.color = color;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }
}
