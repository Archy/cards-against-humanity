package com.tij.game;

import com.tij.models.GameRound;
import com.tij.models.User;
import com.tij.models.json.CardJson;
import com.tij.models.messages.GenericResponse;

import java.time.Duration;
import java.time.LocalDateTime;

import com.tij.models.messages.TimeValidationMessage;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class RoomImpl implements Room {
    private static final int USER_NR = 3;
    private static final int USER_MIN = 2;
    private static final int INIT_CARDS = 4;
    private static final int TIMEOUT = 3000;
    private static final int DEFAULT_INTERVAL = 1500; //in miliseconds: interval between checking if all cards are chosen or the tzar has chosen a winner, etc.
    private static final int SELECT_CARD_TIMEOUT = 30000;
    private static final int SELECT_WINNER_TIMEOUT = 30000;
    private static final int MAX_ROUND_NUMBER = 3;

    @Autowired
    private MessageService messageService;
    @Autowired
    private CardManager cardManager;

    private Map<String, User> players;
    private ArrayList<String> winners;

    private boolean gameStarted;
    private volatile boolean gameFinished;
    private volatile boolean tzarKicked;

    private volatile boolean gameEnd;
    private int roundCounter;

    private GameRound currentGameRound;
    private volatile Timer currenRoundStateTimer;
    private volatile Timer checkUsersTimer;
    
    private int tzarId;
    private String tzarSessionId;
    
    public void setDefaultValues() {
        players = new LinkedHashMap<>();
        winners = new ArrayList<>();
        gameStarted = false;
        gameFinished = false;
        tzarKicked = false;
        gameEnd = false;
        roundCounter = 0;
        currenRoundStateTimer = null;
        checkUsersTimer = null;
        currentGameRound = null;
        tzarId = -1;
        tzarSessionId = "";
    }
    
    @Override
    public GenericResponse registerUser(String sessionId, String name) {
        if(players.isEmpty()) {
            setDefaultValues();
        }
        if(players.size() >= USER_NR ) {
            return new GenericResponse("logged", "There is too many players in the room.");
        }
        if (gameStarted){
            return new GenericResponse("logged", "Game has already started.");
        }
        User user = new User(name, LocalDateTime.now());
        
        if(players.containsKey(sessionId))
            return new GenericResponse("logged", "You are already logged.");
        
        players.put(sessionId, user);        
        this.logUsersLogged();
        
        if(players.size() < USER_NR) {
            sendMessageToAllUsers("wait", Integer.toString(players.size()));
        }
        else {
            gameStarted = true;
            sendMessageToAllUsers("game-start", "Game started");

            if(!startNewRound()) {
                return new GenericResponse("error", "Colud not start new round");
            }
        }
        return new GenericResponse("logged", "yes");
    }
    
    @Override
    public TimeValidationMessage ping(String sessionId, String time, int latency) {
        User user = players.get(sessionId);
        String message;
        if(user == null) 
            message =  "noUser";
        else{
            Long timestamp = System.currentTimeMillis();
            user.setLatency(latency);
            user.setLastPing(LocalDateTime.now());
            message = time + ":" + timestamp.toString();
        }
        //System.out.println("Ping time: " + user.getLastPing() + " | session: " + sessionId + " | login: " + user.getName());
        return new TimeValidationMessage(message,latency);
    }

    private void letTzarChooseWinner() {
        if(currentGameRound.getRoundState() != GameRound.RoundStates.ROUND_STARTED) {
            System.out.println("ERROR: Game has not started! Aborting.");
            return;
        }
        
        HashMap<String, String> cardValuePairs = new HashMap<>();
        
        for(Map.Entry<User, CardJson> entry : currentGameRound.getUserCardMap().entrySet()) {
            cardValuePairs.put(entry.getValue().getId().toString(), entry.getValue().getText());
        }
        
        currentGameRound.setRoundState(GameRound.RoundStates.CARDS_CHOSEN);
        messageService.send(tzarSessionId, new GenericResponse("choose-winner", cardValuePairs));
        waitForTzarChooseWinner(LocalDateTime.now());               
    }
    
    @Override
    public void checkLoggedUsers() {
        checkUsersTimer = new Timer();
        checkUsersTimer.schedule(
        new java.util.TimerTask() {
           @Override
           public void run() {
               //System.out.println("Check for logged users");
               List<String> timedOutPlayer = players.entrySet().stream()
                        .filter(entry -> Duration.between(entry.getValue().getLastPing(), LocalDateTime.now()).toMillis() > TIMEOUT)
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList());
               timedOutPlayer.forEach(RoomImpl.this::kickUser);
           }
        }, 
        TIMEOUT, TIMEOUT);
    }
    private void forceKickUser(String sessionID, String msg) {
        messageService.send(sessionID, new GenericResponse("kicked", msg));
        kickUser(sessionID);
    }

    private synchronized void kickUser(String sessionID) {
        System.out.println("User " + players.get(sessionID).getName() + " removed from game");
        players.remove(sessionID);
        if(gameStarted) {
            if(players.size() >= USER_MIN) {
                if(sessionID.equals(tzarSessionId)) {
                    tzarKicked = true;
                    sendMessageToAllUsers("tzar-kicked", "Tzar was kiced");
                }
                updatePlayerList();
            } else {
                gameFinished = true;
                sendMessageToAllUsers("players-left", "Too many players have left the game");
            }
        } else {
            if(currenRoundStateTimer != null)
                currenRoundStateTimer.cancel();
            sendMessageToAllUsers("wait", Integer.toString(players.size()));
        }
    }

    @Override
    public boolean setUserCard(String sessionId, UUID cardUUID){
        System.out.printf("Selected card sessionID %s carduuid %s\n", sessionId, cardUUID.toString());
        User user = players.get(sessionId);
        CardJson card = cardManager.uuidToCard(cardUUID);
        if(sessionId.equals(tzarSessionId) || user == null || card == null || !user.hasCard(cardUUID))
            return false;
        currentGameRound.relateUserWithCard(user, card);
        return true;
    }

    @Override
    public boolean setWinnerOfRound(String sessionId, UUID cardUUID){
        if(!sessionId.equals(tzarSessionId))
            return  false;
        tzarSessionId = "";
        currentGameRound.setRoundState(GameRound.RoundStates.ROUND_END);

        Optional<User> winner = currentGameRound.getUserCardMap().entrySet().stream()
                .filter(entry -> entry.getValue().getId().equals(cardUUID))
                .map(Map.Entry::getKey)
                .findFirst();
        if(!winner.isPresent())
            return false;
        winner.get().addPoint();

        System.out.println(winner.get().getName() + " has received 1 point");
        return true;
    }
    
    private void sendMessageToAllUsers(String type, String message) {
        for(Map.Entry<String, User> entry : players.entrySet()) {
            messageService.send(entry.getKey(), new GenericResponse(type, message));
        }
    }

    private void sendMessageToAllUsers(String type, Object data) {
        for(Map.Entry<String, User> entry : players.entrySet()) {
            messageService.send(entry.getKey(), new GenericResponse(type, data));
        }
    }

    private void sendRoundStartMsg() {
        for(Map.Entry<String, User> entry : players.entrySet()) {
            messageService.send(entry.getKey(),
                    new GenericResponse(entry.getKey() == tzarSessionId ? "new-round-tzar" : "new-round",
                            currentGameRound)
            );
        }
    }

    private void sendEndMsg() {
        System.out.println(winners.size());
        for(Map.Entry<String, User> entry : players.entrySet()) {
            messageService.send(entry.getKey(),
                    new GenericResponse(winners.contains(entry.getKey()) ? "end-game-win" : "end-game-lost",
                            "Game is over.")
            );
            System.out.println(entry.getKey()+" "+ winners.contains(entry.getKey()) );
        }
    }
    
    private void logUsersLogged() {
        System.out.println("---------------------------------");
        System.out.println("LOGGED USERS:");
        for(Map.Entry<String, User> entry : players.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    private void updatePlayerList() {
        sendMessageToAllUsers(
                "players-list",
                players.values()
                    .stream()
                    .map(User::toJsonModel)
                    .collect(Collectors.toList())
        );
    }

    private void sendWhiteCards() {
        List<Map.Entry<String, User>> playersList = new ArrayList<>(players.entrySet());
        playersList.sort(Comparator.comparingInt(a -> a.getValue().getLatency()));
        System.out.println("Latency list");
        System.out.println(Arrays.toString(playersList.toArray()));
        int lastLatency=0;
        for(Map.Entry<String, User> player : playersList) {
            int whiteObtained = player.getValue().getCards().size();
            
            for(int i = 0; i < (INIT_CARDS - whiteObtained); i++) {
                try {
                    int playerlatency = player.getValue().getLatency();
                    int sleepTime = playerlatency - lastLatency;
                    lastLatency = playerlatency;
                    System.out.println("Sleeping");
                    Thread.sleep(sleepTime);
                    System.out.println("finish sleeping");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    sendWhiteCard(player.getKey());
                }
            }
        }
    }
    
    private String chooseTsar() {
        tzarId = tzarId == -1 ? 0 : ++tzarId%players.size();
        Map.Entry<String, User> user = null;
        int i = 0;
        for(Map.Entry<String, User> entry : players.entrySet()) {
            if(tzarId == i++) {
                user = entry;
                break;
            }
        }
        try {
            System.out.println("Tzar is now: " + user.getValue().getName());
            tzarSessionId = user.getKey();
            return user.getValue().getName();
        }
        catch(NullPointerException e) {
            System.out.println("Tzar not found! ID=" + tzarId);
            return null;
        }
    }

    private void sendWhiteCard(String sessionId) {
        CardJson card = cardManager.getNextWhiteCard();
        players.get(sessionId).addCard(card.getId());
        messageService.send(sessionId, new GenericResponse("new-card", card));
    }

    private boolean startNewRound(){
        if(tzarKicked) {
            --roundCounter;
        } else if (roundCounter==MAX_ROUND_NUMBER){
            gameEnd=true;
            return false;
        }
        if(gameFinished)  {
            players.clear();
            gameStarted = false;
            gameFinished = false;
            tzarKicked = false;

            cardManager.reset();

            return false;
        }
        if(currentGameRound!=null && !tzarKicked) {
            currentGameRound.getUserCardMap().forEach((key, value) -> {
                key.removeCard(value.getId());
            });
        }
        tzarKicked = false;

        updatePlayerList();
        sendWhiteCards();
        try{
            currentGameRound = new GameRound(roundCounter++);

            CardJson blackcard = cardManager.getNextBlackCard();
            currentGameRound.setBlackCard(blackcard);
            currentGameRound.setTzar(chooseTsar());
            sendRoundStartMsg();

            currentGameRound.setRoundState(GameRound.RoundStates.ROUND_STARTED);
            waitForAllCardsChosen(LocalDateTime.now());
            return true;
        }
        catch (Exception ex) {
            System.out.println("!!! ROUND NOT STARTED !!!");
            return false;
        }
    }

    private void waitForAllCardsChosen(LocalDateTime taskStartTime) {
        currenRoundStateTimer = new Timer();
        currenRoundStateTimer.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        //System.out.println("Check if all users have chosen white card: "+ currentGameRound.getUserCardMap().size() + " / " + players.size());
                        if (gameFinished || tzarKicked) {
                            currenRoundStateTimer.cancel();
                            currenRoundStateTimer.purge();
                            startNewRound();
                        }
                        if (currentGameRound.getUserCardMap().size() == players.size() - 1) {
                            currenRoundStateTimer.cancel();
                            currenRoundStateTimer.purge();
                            System.out.println("All players have chosen cards.");
                            letTzarChooseWinner();
                        }
                        if (Duration.between(taskStartTime, LocalDateTime.now()).toMillis() > SELECT_CARD_TIMEOUT) {
                            currenRoundStateTimer.cancel();
                            currenRoundStateTimer.purge();
                            System.out.println("Time for choosing white cards is over.");
                            //kick players that didn't choose
                            List<String> timedOutPlayers = players.entrySet().stream()
                                    .filter(entry -> !currentGameRound.getUserCardMap().containsKey(entry.getValue()))
                                    .map(Map.Entry::getKey)
                                    .filter(sessionId -> !sessionId.equals(tzarSessionId))
                                    .collect(Collectors.toList());
                            timedOutPlayers.forEach(sessionId -> forceKickUser(sessionId, "You have been kicked out of the game"));
                            letTzarChooseWinner();
                        }
                        if (gameEnd){
                            currenRoundStateTimer.cancel();
                            currenRoundStateTimer.purge();
                            finishGame();
                        }
                    }

                },
                DEFAULT_INTERVAL, DEFAULT_INTERVAL);
    }
    
    private void waitForTzarChooseWinner(LocalDateTime taskStartTime) {
        currenRoundStateTimer = new Timer();
        currenRoundStateTimer.schedule(
        new java.util.TimerTask() {
            @Override
            public void run() {
                    if (gameFinished || tzarKicked) {
                        currenRoundStateTimer.cancel();
                        currenRoundStateTimer.purge();
                        startNewRound();
                    }
                    if (currentGameRound.getRoundState() == GameRound.RoundStates.ROUND_END) {
                        currenRoundStateTimer.cancel();
                        currenRoundStateTimer.purge();
                        System.out.println("Time to start new round. Round counter: "+roundCounter);
                        startNewRound();
                    }
                    if (Duration.between(taskStartTime, LocalDateTime.now()).toMillis() > SELECT_WINNER_TIMEOUT) {
                        currenRoundStateTimer.cancel();
                        currenRoundStateTimer.purge();
                        System.out.println("Time for choosing white cards is over.");
                        //sendMessageToAllUsers("round-end-error", "Tzar has not chosen winner.");
                        forceKickUser(tzarSessionId, "You have been kicked out of the game");
                        startNewRound();
                    }
                if (gameEnd){
                    currenRoundStateTimer.cancel();
                    currenRoundStateTimer.purge();
                    finishGame();
                }

            }
        }, 
        DEFAULT_INTERVAL, DEFAULT_INTERVAL);
    }
    private void finishGame(){
        Iterator<User> it = players.values().iterator();
        int topScore=0;
        while (it.hasNext()){
            User u = it.next();
            if (u.getPoints() >= topScore)
                topScore=u.getPoints();
        }

        for (Map.Entry<String, User> entry : players.entrySet()){
                if (entry.getValue().getPoints() == topScore) {
                    winners.add(entry.getKey());
                    System.out.println(entry.getValue().getName());
                }
            }

        System.out.println("Game is over.");
        updatePlayerList();
        sendEndMsg();
        
        setDefaultValues();
    }
}
