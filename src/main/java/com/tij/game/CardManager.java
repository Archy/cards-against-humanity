package com.tij.game;

import com.tij.models.json.CardJson;

import java.util.Optional;
import java.util.UUID;

public interface CardManager {
    void reset();

    CardJson getNextWhiteCard();
    CardJson getNextBlackCard();

    CardJson uuidToCard(UUID uuid);
}
