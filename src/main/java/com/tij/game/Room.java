package com.tij.game;

import com.tij.models.messages.GenericResponse;
import com.tij.models.messages.TimeValidationMessage;

import java.util.UUID;

public interface Room {
    GenericResponse registerUser(String sessionId, String name);

    TimeValidationMessage ping(String sessionId, String name, int latency);

    void checkLoggedUsers();

    public void setDefaultValues();
    
    boolean setUserCard(String sessionId, UUID cardUUID);
    
    boolean setWinnerOfRound(String sessionId, UUID cardUUID);
}
