package com.tij.game;

import com.tij.models.messages.GenericResponse;

public interface MessageService {
    void send(final String sessionId, final GenericResponse genericResponse);
}
