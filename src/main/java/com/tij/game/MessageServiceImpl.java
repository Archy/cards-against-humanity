package com.tij.game;

import com.tij.models.messages.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

@Component
public class MessageServiceImpl implements MessageService {
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    public void send(final String sessionId, final GenericResponse genericResponse) {
        final SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        headerAccessor.setContentType(MimeTypeUtils.APPLICATION_JSON);
        messagingTemplate.convertAndSendToUser(sessionId,"/cah/socket",
                genericResponse, headerAccessor.getMessageHeaders());
    }
}
