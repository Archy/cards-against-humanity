package com.tij.game;

import com.tij.data.CardsRepository;
import com.tij.models.Card;
import com.tij.models.json.CardJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Component
public class CardManagerImpl implements CardManager {
    @Autowired
    private CardsRepository cardsRepository;

    private final Random generator = new Random();

    private final Map<UUID, Card> cards = new HashMap<>();
    private List<Long> remainingWhiteCards;
    private List<Long> remainingBlackCards;


    @PostConstruct
    public void setUp() {
        reset();
    }

    public void reset() {
        remainingWhiteCards = cardsRepository.getAllWhiteIds();
        remainingBlackCards = cardsRepository.getAllBlackIds();
        cards.clear();
    }

    @Override
    public CardJson getNextWhiteCard() {
        long id = remainingWhiteCards.get(generator.nextInt(remainingWhiteCards.size()));
        remainingWhiteCards.remove(id);
        return cardToJson(id);
    }

    @Override
    public CardJson getNextBlackCard() {
        long id = remainingBlackCards.get(generator.nextInt(remainingBlackCards.size()));
        remainingWhiteCards.remove(id);
        return cardToJson(id);
    }

    private CardJson cardToJson(long id) {
        UUID uuid = UUID.randomUUID();
        Card card = cardsRepository.findById(id);
        cards.put(uuid, card);
        return new CardJson(uuid, card.getText(), card.getColor());
    }

    /*@Override
    public Optional<CardJson> uuidToCard(UUID uuid) {
        Card card = cards.get(uuid);
        if(card == null)
            return Optional.empty();

        return Optional.of(new CardJson(uuid, card.getText(), card.getColor()));
    }*/

    @Override
    public CardJson uuidToCard(UUID uuid) {
        Card card = cards.get(uuid);
        if(card == null)
            return null;
        return new CardJson(uuid, card.getText(), card.getColor());
    }
}
