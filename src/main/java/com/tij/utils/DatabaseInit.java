package com.tij.utils;

import com.tij.data.CardsRepository;
import com.tij.models.Card;
import com.tij.models.CardColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * DevOps card set
 * https://github.com/bridgetkromhout/devops-against-humanity
 * https://docs.google.com/spreadsheets/d/1OKgmjNz8l7skYfrbrOtYT6QmSf_y-BzZOWJWZC3tbUQ/edit#gid=0
 */
@Component
public class DatabaseInit {
    @Autowired
    private CardsRepository cardsRepository;

    @PostConstruct
    public void initializeDatabase() {
        if(cardsRepository.findAll().isEmpty()) {
            for(String text : WhiteCardsData.cards) {
                cardsRepository.save(new Card(text, CardColor.WHITE));
            }

            for(String text : BlackCardsData.cards) {
                cardsRepository.save(new Card(text, CardColor.BLACK));
            }
        }
    }
}
