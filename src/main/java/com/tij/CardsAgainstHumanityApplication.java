package com.tij;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardsAgainstHumanityApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardsAgainstHumanityApplication.class, args);
	}
}
