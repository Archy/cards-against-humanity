package com.tij.controllers;

import com.tij.game.Room;
import com.tij.game.RoomImpl;
import com.tij.models.messages.CardMessage;
import com.tij.models.messages.GenericResponse;
import com.tij.models.messages.RoomEntryRequest;
import javax.annotation.PostConstruct;

import com.tij.models.messages.TimeValidationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class IndexController {

    @Autowired
    private Room room;

    @PostConstruct
    public void initialize(){
        room.setDefaultValues();
        room.checkLoggedUsers();
    }

    @MessageMapping("/entry")
    @SendToUser("/cah/socket")
    public GenericResponse entryRoom(SimpMessageHeaderAccessor messageHeaderAccessor, RoomEntryRequest request){
        return room.registerUser(messageHeaderAccessor.getSessionId(), request.getUserName());
    }

    @MessageMapping("/ping")
    @SendToUser("/cah/socket")
    public TimeValidationMessage ping(SimpMessageHeaderAccessor messageHeaderAccessor, TimeValidationMessage request){
        return room.ping(messageHeaderAccessor.getSessionId(), request.getTime(),request.getMyCurrentLatency());
    }

    @MessageMapping("/selected")
    @SendToUser("/cah/socket")
    public boolean setSelectedCard(SimpMessageHeaderAccessor messageHeaderAccessor, CardMessage message){
        return room.setUserCard(messageHeaderAccessor.getSessionId(), message.getCarduuid());
    }
    
    @MessageMapping("/selected-winner")
    @SendToUser("/cah/socket")
    public boolean setWinnerOfRound(SimpMessageHeaderAccessor messageHeaderAccessor, CardMessage message){
        return room.setWinnerOfRound(messageHeaderAccessor.getSessionId(), message.getCarduuid());
    }
}
