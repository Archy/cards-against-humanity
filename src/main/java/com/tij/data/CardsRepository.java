package com.tij.data;

import com.tij.models.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepository extends JpaRepository<Card, Long> {
    @Query("SELECT c FROM Card c WHERE c.id=:id AND c.color=com.tij.models.CardColor.BLACK")
    Card getBlackCardById(@Param("id") long id);

    @Query("SELECT c FROM Card c WHERE c.id=:id AND c.color=com.tij.models.CardColor.WHITE")
    Card getWhiteCardById(@Param("id") long id);

    @Query("SELECT c.id FROM Card c WHERE c.color=com.tij.models.CardColor.WHITE")
    List<Long> getAllWhiteIds();

    @Query("SELECT c.id FROM Card c WHERE c.color=com.tij.models.CardColor.BLACK")
    List<Long> getAllBlackIds();

    Card findById(long id);
}
